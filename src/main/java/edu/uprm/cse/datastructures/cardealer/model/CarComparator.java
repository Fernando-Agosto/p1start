package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {
	//Concatenates the brand, model and model option of both cars and then compares them
	public int compare (Car obj1, Car obj2) {
		String car1 = obj1.getCarBrand()+obj1.getCarModel()+obj1.getCarModelOption();
		String car2 = obj2.getCarBrand()+obj2.getCarModel()+obj2.getCarModelOption();
		return car1.compareTo(car2);
		
	}

}
