package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	// valid position in the list
	// [0, size() - 1]	
	private int size;
	private DNode<E> header;
	private Comparator<E> comp;

	public CircularSortedDoublyLinkedList() {
		this.size = 0;
		this.header = new DNode<E>();
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.comp = new DefaultComparator();
	}

	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this();
		this.comp = comp;
	}

	@Override
	public Iterator<E> iterator() {
		return new CSDLLIterator();
	}

	@Override
	public boolean add(E obj) {
		//checks that the object is not null
		if(obj == null) {
			return false;
		}
		
		DNode<E> temp = new DNode<E>(obj);
		//uses the created temporary variable and if the list is empty
		//it inserts the node in the list attaching the header 
		//as its next and previous
		if(this.isEmpty()) {
			this.header.setNext(temp);
			this.header.setPrev(temp);
			temp.setNext(this.header);
			temp.setPrev(this.header);
			this.size++;
			return true;
		}

		DNode<E> target = header.getNext();
		while(target != header) {
			//as long as the target variable is not the header; 
			//meaning as long as it doesn't loop around, compares if
			//the temporary variable is greater, lesser or equal
			//and organizes them accordingly; alphabetically
			if(comp.compare(obj,  target.getElement()) <= 0) {
				temp.setNext(target);
				temp.setPrev(target.getPrev());
				target.setPrev(temp);
				temp.getPrev().setNext(temp);
				this.size++;
				return true;
			}

			target = target.getNext();
		}
		//if the element is greater it places it at the end of the list
		temp.setNext(this.header);
		temp.setPrev(this.header.getPrev());
		this.header.setPrev(temp);
		temp.getPrev().setNext(temp);
		size++;
		return true;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean remove(E obj) {
		if(this.contains(obj)) {
			return this.remove(this.firstIndex(obj));
		}
		return false;
	}

	@Override
	public boolean remove(int index) {
		if(index > size-1 || index<0) {
			throw new IndexOutOfBoundsException();
		}
		//searches for the ntr; Node To Remove, which will be at the end
		//of then this loop 
		DNode<E> ntr = this.header.getNext();
		for (int i = 0; i < index; i++) {
			ntr = ntr.getNext();
		}
		//gets the previous node of the Node To Remove and 
		//assigns it to a variable; prev
		
		//gets the next node of the Node To Remove and 
		//assigns it to a variable; next
		
		//sets the next node of prev to the variable next
		//sets the previous node of next to the variable prev
		//clears the Node To Remove to help the garbage collector
		DNode<E> prev = ntr.getPrev();
		DNode<E> next = ntr.getNext();
		prev.setNext(next);
		next.setPrev(prev);
		ntr.clear();

		size--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		int firstIndex = this.firstIndex(obj);
		int lastIndex = this.lastIndex(obj);
		int currSize = this.size();
		
		//goes through the list starting from the first index of the object
		//and removes the object, this process is repeated until
		//all given objects are eliminated from the list
		for(int i = firstIndex; i<=lastIndex; i++) {
			this.remove(firstIndex);
		}
		return currSize - this.size();
	}

	@Override
	public E first() {
		return header.getNext().getElement();
	}

	@Override
	public E last() {
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		DNode<E> ntr = this.header.getNext();
		for (int i = 0; i < index; i++) {
			ntr = ntr.getNext();
		}

		return ntr.getElement();
	}

	@Override
	public void clear() {
		DNode<E> target = header.getNext();
		//as long as the target variable is not the header; 
		//meaning as long as it doesn't loop around, it clears
		//the node the ntc; Node To Clear
		while(target != header) {
			DNode<E> ntc = target;
			target = target.getNext();
			ntc.clear();
		}
		//makes sure the header points to itself
		//by setting itself to be its next and previous
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.size = 0;
	}

	@Override
	public boolean contains(E e) {
		if(this.firstIndex(e)>=0)return true;
		return false;
	}

	@Override
	public boolean isEmpty() {
		return size == 0 && header.getNext() == header && header.getPrev() == header;
	}

	@Override
	public int firstIndex(E e) {
		int counter = 0;

		DNode<E> temp = this.header.getNext();
		while(temp != this.header) {
			if(temp.getElement().equals(e)){
				return counter;
			}
			temp = temp.getNext();
			counter++;
		}

		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int counter = this.size-1;

		DNode<E> temp = this.header.getPrev();
		while(temp != this.header){
			if(temp.getElement() == e) {
				return counter;
			}
			temp = temp.getPrev();
			counter--;
		}

		return -1;
	}

	public void setComparator(Comparator<E> comp) {
		this.comp = comp;
	}

	private class DNode<T> implements Node<T>{
		private DNode<T> prev;
		private DNode<T> next;
		private T e;

		public DNode(T e, DNode<T> prev, DNode<T> next){
			this.e = e;
			this.prev = prev;
			this.next = next;
		}
		public DNode(T e) {
			this(e, null, null);
		}

		public DNode() {
			this(null, null, null);
		}

		public void clear() {
			this.setElement(null);
			this.setNext(null);
			this.setPrev(null);
		}

		public DNode<T> getPrev(){
			return prev;
		}

		public DNode<T> getNext(){
			return next;
		}

		public T getElement() {
			return e;
		}

		public void setPrev(DNode<T> node){
			this.prev = node;
		}

		public void setNext(DNode<T> node){
			this.next = node;
		}

		public void setElement(T e) {
			this.e = e;
		}
	}

	private class DefaultComparator implements Comparator<E>{

		@Override
		public int compare(E o1, E o2) {
			String obj1 = o1.toString();
			String obj2 = o2.toString();
			return obj1.compareTo(obj2);
		}

	}

	private class CSDLLIterator implements Iterator<E>{
		private DNode<E> current;

		protected CSDLLIterator() {
			current = header.getNext();
		}
		@Override
		public boolean hasNext() {
			return current.getNext() != header.getNext();
		}

		@Override
		public E next() {
			if(!hasNext()) throw new NoSuchElementException("No more elements");
			E etr = current.getElement();
			current = current.getNext();
			return etr;
		}

	}
}