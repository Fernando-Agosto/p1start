package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {
	private final SortedList<Car> carList = CarList.getInstance();

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		//goes trough the carList and inserts them into an array

		//returns a HTTP status code of 200 (OK) if performed successfully 
		Car[] carArray = new Car[carList.size()];
		for (int i = 0; i < carList.size(); i++) {
			carArray[i]=carList.get(i);
		}
		return carArray;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		//goes through the carList checking if the car matches the unique id
		//given as parameter

		//returns a HTTP status code of 200 (OK) if performed successfully
		//or returns response HTTP status code of 404 (Not Found)
		for (int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId()==id) {
				return carList.get(i);
			}
		}
		throw new NotFoundException();
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		//checks that the car is not already in the list
		//if already on the list or if not successfully added return (BAD REQUEST)
		//returns a HTTP status code of 201 if successfully added the car
		for (int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == car.getCarId())
				return Response.status(Response.Status.BAD_REQUEST).build();
		}

		if(carList.add(car)) {
			return Response.status(201).build();
		}

		return Response.status(Response.Status.BAD_REQUEST).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(@PathParam("id") long id, Car car){
		//checks the carList for a car with an equal id
		//if found it replaces the old car with the new car and 
		//returns HTTP status code of 200 (OK)
		//otherwise it returns response HTTP status code of 404 (Not Found)
		if(car.getCarId() != id)
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();

		for(int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == car.getCarId()) {
				if(carList.remove(carList.get(i)) && carList.add(car))
					return Response.status(Response.Status.OK).build();
			}
		}

		return Response.status(Response.Status.NOT_FOUND).build();

	}    

	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		//checks the carList for a car with an equal id
		//if found it removes the car from the carList and
		//returns HTTP status code of 200 (OK)
		//otherwise it returns response HTTP status code of 404 (Not Found)
		for(int i = 0; i < carList.size(); i++) {
			if(carList.get(i).getCarId() == id) {
				if(carList.remove(carList.get(i)))
					return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

}
